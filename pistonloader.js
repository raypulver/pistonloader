'use strict';

const fs = require('fs');
const path = require('path');
const runtime = require('./lib/runtime');

const PISTON_PATH = path.join(__dirname, 'lib', 'pistons');
const arrayDiff = require('./lib/util/array-diff');
const mapSeries = require('./lib/util/map-series');
const ln = (v) => ((console.log(v)), v);

const bootstrap = async (pistons = []) => {
  const builtIns = fs.readdirSync(PISTON_PATH).map((v) => path.parse(v).name);
  const truePistonList = arrayDiff(builtIns, pistons.map(([ name ]) => name)).map((defaultBuiltIn) => [ defaultBuiltIn, {} ]).concat(pistons.map((v) => typeof v === 'string' ? [ v, {} ] : v));
  await mapSeries(truePistonList, async ([ pistonName, cfg ]) => {
    let piston;
    try {
      console.log('loading ' + pistonName + '...');
      if (builtIns.includes(pistonName)) piston = require(path.join(PISTON_PATH, pistonName));
      else piston = require(pistonName);
      await piston.load(cfg);
      console.log('loaded ' + pistonName);
    } catch (e) {
      console.error('failed to load ' + pistonName);
      console.error(e.stack);
    }
  });
  return runtime.getRuntime();
};

Object.assign(module.exports, {
  bootstrap
});
