'use strict';

const ErrorStackParser = require('error-stack-parser');
const {
  Namespace,
  NamespaceMixin
} = require('./namespace');

const rewriteNamespace = (s) => s === '' ? 'global' : s === 'global' ? '' : s;

let rt;
const util = require('util');
const { custom } = util.inspect;

const pistons = Namespace.create();

const runtimeProps = {
  name: 'runtime'
};

const namespaces = Namespace.create({
  global: Namespace.create()
});

let context = [];

const runtimeImpl = {
  getContext() {
    return context[0] || {
      identifier: '::unknown',
      filename: 'unknown',
      args: []
    };
  },
  getPistons() { return pistons; },
  getPiston(pistonName) { return pistons.get(pistonName) || null; },
  registerPiston(piston) {
    return pistons.register(piston.name || '', piston);
  },
  unregisterPiston(piston) {
    return pistons.deleteName(piston.name);
  },
  createNamespace(namespace) {
    if (namespaces.has(namespace)) throw Error('namespace ' + namespace + ' already exists');
    return (namespaces[namespace] = Namespace.create());
  },
  getNamespace(namespace) {
    return namespaces.get(namespace);
  },
  deleteNamespace(namespace) {
    namespaces.deleteName(namespace);
    return this;
  },
  _createHookedFunctionAndBackup(fn, ...args) {
    const retval = runtimeImpl._createHookedFunction(fn, ...args);
    retval.getOriginalFunction = () => fn;
    return retval;
  },
  _createHookedFunction(fn, hook, hookActiveGetter, beforeOrAfter = 'before') {
    if (beforeOrAfter === 'before') return function (...args) {
      if (hookActiveGetter()) args = hook(...args);
      if (typeof args === 'object' && typeof args.then === 'function') return args.then((realArgs) => fn.apply(this, realArgs));
      return fn.apply(this, args);
    };
    if (beforeOrAfter === 'after') return function (...args) {
      const retval = fn.apply(this, args);
      if (typeof retval === 'object' && typeof retval.then === 'function') return retval.then((realRetval) => hook(realRetval, ...args));
      return hook(retval, ...args);
    };
    const { logger } = pistons;
    const msg = 'must specify "before" or "after" as the fourth argument, got ' + String(beforeOrAfter);
    if (!logger) throw Error(msg);
    return logger.logErrorWithStack(msg);
  },
  _hook(fn, hook, beforeOrAfter) {
    const [ namespaceName, method ] = fn.split('::').map(rewriteNamespace);
    const msg = 'function ' + method + ' not found in namespace ' + namespaceName;
    const { logger } = pistons;
    let namespace, piston, hookActive = true;
    if ((namespace = namespaces.get(namespaceName))) {
      if (!namespace.has(method)) {
        if (!logger) throw Error(msg);
        return logger.logErrorWithStack(msg);
      }
      const fn = namespace.get(method);
      namespace.deleteName(method);
      namespace.register(method, runtimeImpl._createHookedFunctionAndBackup(fn, hook, () => hookActive, beforeOrAfter));
    } else if ((piston = pistons.get(namespaceName))) {
      const fn = piston[method];
      if (!fn) {
        if (!logger) throw Error(msg);
        return logger.logErrorWithStack(msg);
      }
      delete piston[method];
      piston[method] = runtimeImpl._createHookedFunctionAndBackup(fn, hook, () => hookActive, beforeOrAfter);
    }
    return () => {
      if (hookActive) {
        hookActive = false;
        return true;
      }
      return false;
    };
  },
  hookBefore(fn, hook) {
    return runtimeImpl._hook(fn, hook, 'before');
  },
  hookAfter(fn, hook) {
    return runtimeImpl._hook(fn, hook, 'after');
  },
  pushContext(identifier, filename, args) {
    context.unshift({
      identifier,
      filename,
      args
    });
    return this;
  },
  popContext() {
    context.shift();
    return this;
  },
  call(fn, ...args) {
    const [ piston, method ] = fn.split('::').map(rewriteNamespace);
    const pistonObject = pistons.get(piston);
    const namespaceObject = namespaces.get(piston);
    const { logger } = pistons;
    if (namespaceObject !== undefined) {
      const msg = 'method ' + method + ' not found in namespace ' + piston;  
      const namespaceFn = namespaceObject[method];
      if (!namespaceFn) {
        if (!logger) throw Error(msg);
        try {
          throw Error(msg);
        } catch (e) {
          logger.logError(e.stack);
          return;
        }
      }
      return namespaceFn(...args);
    }
    if (!pistonObject) {
      const msg = 'namespace ' + piston + ' not found';
      if (!logger) throw Error(msg);
      try {
        throw Error(msg);
      } catch (e) {
        logger.logError(e.stack);
        return;
      }
    }
    return pistonObject[method](...args);
  },
  registerGlobalFunction(name, fn) {
    return namespaces.get('global').register(name, fn);
  },
  unregisterGlobalFunction(name) {
    return namespaces.get('global').deleteName(name);
  },
  async reloadPiston(name) {
    const piston = runtimeImpl.getPiston(name);
    if (!piston) return false;
    const path = piston.getPath();
    await piston.unload();
    const reloadedPiston = require(path);
    await reloadedPiston.load();
  }
};

const createRuntime = () => new Proxy({}, {
  get: (_, prop) => {
    if (prop === 'inspect' || prop === custom) return (v) => '<Runtime: Pistons(' + Object.keys(pistons).length + ')>';
    if (runtimeProps[prop] !== undefined) return runtimeProps[prop];
    if (runtimeImpl[prop] !== undefined) return runtimeImpl[prop];
    const globalProp = namespaces.get('global').get(prop);
    if (globalProp !== undefined) return globalProp;
    if (prop === 'then' || prop === 'catch') return undefined;
    return function unimplemented() {
      const logger = runtimeImpl.getPiston('logger');
      const msg = 'function ' + prop + ' not registered with runtime';
      if (!logger) throw Error(msg);
      logger.logError(new Error(msg).stack);
    };
  }
});

const initializeRuntime = () => (rt = createRuntime());

const getRuntime = () => rt || initializeRuntime();

Object.assign(module.exports, {
  initializeRuntime,
  getRuntime
});
