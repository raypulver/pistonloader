'use strict';

const { hasOwnProperty } = Object.prototype;

const flattenPrototypeChain = (proto) => {
  const retval = Object.create(null);
  do {
    Object.getOwnPropertyNames(proto).forEach((v) => {
      let o = retval;
      while (hasOwnProperty.call(o, v)) {
        if (!Object.getPrototypeOf(o)) Object.setPrototypeOf(o, Object.create(null));
        o = Object.getPrototypeOf(o);
      }
      o[v] = proto[v];
    }, {});
  } while ((proto = Object.getPrototypeOf(proto)));
  return retval;
};

Object.assign(module.exports, {
  flattenPrototypeChain
});
