'use strict';

const { keyMirrorFromAry, keyMirror } = require('./util/key-mirror');
const { flattenPrototypeChain } = require('./proto');
const { assignOwnProps } = require('./util/assign-own-props');

function PistonClass(constructor, name = 'Anonymous') {
  if (!(this instanceof PistonClass)) return new PistonClass(constructor, name);
  assignOwnProps(this, constructor.prototype);
  this._className = constructor.name || name;
  this._mixins = {};
  this._methods = {};
  this._cacheInterface();
}

Object.assign(PistonClass.prototype, {
  getClassName() {
    return this._className;
  },
  getMethods() {
    return this._methods;
  },
  _cacheInterface() {
    this._methods = keyMirrorFromAry(Object.keys(flattenPrototypeChain(this)));
  },
  getMixins() {
    return this._mixins;
  },
  applyMixin(mixin) {
    const stripped = Object.assign({}, mixin);
    const { name } = stripped;
    delete stripped.name;
    Object.assign(this, stripped);
    this.getMixins()[name] = name;
    this._cacheInterface();
    return this;
  },
  create(...args) {
    const { constructor } = this;
    const retval = Object.create(this);
    constructor.apply(retval, args);
    return retval;
  }
});

Object.assign(module.exports, {
  PistonClass
});
