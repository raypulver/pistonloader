'use strict';

const { PistonClass } = require('./class');
const runtime = require('./runtime');
const noop = require('./util/noop');
const ErrorStackParser = require('error-stack-parser');
const mapOwn = require('./util/map-own');
const { NamespaceMixin } = require('./namespace');

const requiredFns = ['load', 'unload'];
const { cache } = require;

const Piston = PistonClass(function Piston(impl) {
  const stripped = Object.assign({}, impl);
  const { load, unload } = stripped;
  delete stripped.load;
  delete stripped.unload;
  this._runtime = runtime.getRuntime();
  Object.assign(this, mapOwn(stripped, (fn, name) => typeof fn === 'function' ? Object.assign(function (...args) {
    this.getRuntime().pushContext((impl.name || '') + '::' + name, this.getPath(), args);
    const retval = fn.apply(this, args);
    this.getRuntime().popContext();
    return retval;
  }, {
    identifier: (impl.name || '') + '::' + name
  }) : fn));
  const tmp = { load, unload };
  requiredFns.forEach((name) => Object.defineProperty(this, '_' + name, {
    value: tmp[name] || noop,
    enumerable: false,
    configurable: true,
    writable: true
  }));
});

const { create: createOriginal } = Piston;

Piston.create = (...args) => {
  const retval = createOriginal.apply(Piston, args);
  const stack = ErrorStackParser.parse(new Error());
  const i = stack.findIndex((v) => v.functionName.match('PistonClass.Piston.create.args'));
  retval.__filename = (stack[i + 1] || {}).fileName;
  return retval;
};

Piston.applyMixin({
  name: 'piston-core',
  getPath() {
    return this.__filename;
  },
  getRuntime() {
    return this._runtime;
  },
  getSiblingPiston(pistonName) {
    return this.getRuntime().getPiston(pistonName);
  },
  async load(cfg) {
    const resolved = require.resolve(this.getPath());
    if (cache[resolved] === undefined) cache[resolved] = this;
    const rt = this.getRuntime();
    if (rt.getPiston(this.name)) {
      const logger = rt.getPiston('logger');
      const msg = 'failed to load ' + this.name + ' -- a piston by this name has already been loaded';
      if (!logger) throw Error(msg);
      logger.logError(msg);
      return false;
    }
    await this._load(cfg);
    rt.registerPiston(this);
    return true;
  },
  async unload() {
    const rt = this.getRuntime();
    if (!rt.getPiston(this.name)) {
      const logger = rt.getPiston('logger');
      const msg = 'failed to unload ' + this.name + ' -- no piston by this name has been loaded';
      if (!logger) throw Error(msg);
      logger.logError(msg);
      return false;
    }
    await this._unload();
    rt.unregisterPiston(this);
    delete cache[require.resolve(this.getPath())];
    return true;
  }
});

Piston.applyMixin(NamespaceMixin);

Object.assign(module.exports, {
  Piston
});
