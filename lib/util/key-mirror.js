'use strict';

const keyMirror = (o) => Object.keys(o).reduce((r, v) => {
  r[v] = v;
  return r;
}, {});

const keyMirrorFromAry = (ary) => keyMirror(ary.reduce((r, v) => {
  r[v] = null;
  return r;
}, {}));

Object.assign(module.exports, {
  keyMirror,
  keyMirrorFromAry
});
