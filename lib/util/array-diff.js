'use strict';

module.exports = (a, b) => a.filter((v) => !b.includes(v));
