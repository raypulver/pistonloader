'use strict';

const assignOwnProps = (o, props) => {
  Object.getOwnPropertyNames(props).forEach((v) => {
    o[v] = props[v];
  });
  return o;
};

Object.assign(module.exports, {
  assignOwnProps
});
