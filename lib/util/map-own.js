'use strict';

module.exports = function mapOwn(o, fn) {
  return Object.keys(o).reduce((r, v) => {
    r[v] = fn.call(this, o[v], v, o);
    return r;
  }, {});
};
