'use strict';

module.exports = async (ary, fn) => {
  const retval = [];
  for (let i = 0; i < ary.length; ++i) {
    retval.push(await fn(ary[i], i, ary));
  }
  return retval;
};
