'use strict';

module.exports = (newObj, oldObj) => [...new Set([ ...Object.keys(newObj), ...Object.keys(oldObj) ])].reduce((r, v) => {
  if (newObj[v] === oldObj[v]) return r;
  r[v] = [ newObj[v], oldObj[v] ];
  return r;
}, {});
