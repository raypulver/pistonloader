'use strict';

const { PistonClass } = require('./class');

const Namespace = PistonClass(function Namespace(o) {
  Object.assign(this, o);
});

const NamespaceMixin = {
  name: 'namespace-core',
  deleteName(name) {
    delete this[name];
    return this;
  },
  get(name) {
    return this[name];
  },
  register(name, fn) {
    this[name] = fn;
    return this;
  },
  has(name) {
    return this[name] !== undefined;
  },
  clear() {
    Object.keys(this).forEach((v) => {
      delete this[v];
    });
  }
};

Namespace.applyMixin(NamespaceMixin);

Object.assign(module.exports, {
  Namespace,
  NamespaceMixin
});
