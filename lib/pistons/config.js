'use strict';

const { Piston } = require('../piston');

module.exports = Piston.create({
  name: 'config',
  supportedBackends: [ 'memory', 'db', 'redis' ],
  getBackendType() {
    return this._backendType;
  },
  load({ backend = 'memory' } = {}) {
    if (!this.supportedBackends.includes(backend)) throw Error('unsupported backend: ' + backend);
    this._backendType = backend;
    if (backend === 'memory') this._data = {};
  },
  async get(k) {
    switch (this.getBackendType()) {
      case 'memory':
        return this._data[k];
      case 'redis':
    }    
    if (k === undefined) return this._data;
    return this._data[k];
  },
  async set(moduleName, k, v) {
  },
  subscribeToConfigChange(name, fn) {
  }
});  
