'use strict';

const { Piston } = require('../piston');
const chalk = require('chalk');

module.exports = Piston.create({
  name: 'logger',
  load: () => {},
  unload: () => {},
  setWarningLevel(level) {
    this._level = level;
    return this;
  },
  getWarningLevel() {
    return this._level;
  },
  logFatal(msg) {
    console.error(msg);
    return msg;
  },
  logError(msg) {
    console.error(msg);
    return msg;
  },
  logInfo(msg) {
    console.log(msg);
    return msg;
  },
  logWarning(msg, level) {
    if (level >= this.getWarningLevel()) console.warn(msg);
    return msg;
  },
  logErrorWithStack(msg) {
    try {
      throw new Error(msg);
    } catch (e) {
      this.logError(e.stack);
    }
  }
});
